#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

static const float SINEWAVE_FREQUENCY_HZ = 50.0f;
static const float SAMPLING_FREQUENCY_HZ = 1000.0f;
static const float TEST_DURATION_S = 1.0f;

static const float AMPLITUDE_VOLTS_RMS = 230.0f;
static const float TEST_SAMPLE_COUNT = TEST_DURATION_S * SAMPLING_FREQUENCY_HZ;
static const float AMPLITUDE_VOLTS_PEAK = AMPLITUDE_VOLTS_RMS * sqrtf(2.0f);

template<typename T>
static T SineWave(void)
{
    static T PhaseAccumulator = (T)0.0;
    T VoltageV = sinl((long double)PhaseAccumulator) * AMPLITUDE_VOLTS_PEAK;
    PhaseAccumulator += (T)2.0 * (T)M_PI * (T)SINEWAVE_FREQUENCY_HZ / (T)SAMPLING_FREQUENCY_HZ;
    return VoltageV;
}

int main()
{
    float fVoltageV = 0.0f;
    double dVoltageV = 0.0;
    long double ldVoltageV = (long double)0.0;
    int SampleCount;
    printf("Hello God and the world!\n");
    printf("Using GCC %d.%d.%d, C++ standard %ld\n", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__, __cplusplus);
    static_assert(sizeof(float) >= 4, "Size of 'float' smaller than expected!");
    static_assert(sizeof(double) >= 8, "Size of 'double' smaller than than expected!");
    static_assert(sizeof(long double) >= 12, "Size of 'long double' smaller than than expected!");
    printf("A sine wave is generated with a frequency of %.2fHz and a peak amplitude of %.4fV.\n",
           SINEWAVE_FREQUENCY_HZ, AMPLITUDE_VOLTS_PEAK);
    printf("The sampling frequency is %.2fHz and the test duration is %.3fs.\n",
           SAMPLING_FREQUENCY_HZ, TEST_DURATION_S);
    for (SampleCount = 0; SampleCount <= (int)TEST_SAMPLE_COUNT; SampleCount++)
    {
        fVoltageV = SineWave<float>();
        dVoltageV = SineWave<double>();
        ldVoltageV = SineWave<long double>();
        if ( (SampleCount <= (SAMPLING_FREQUENCY_HZ / SINEWAVE_FREQUENCY_HZ / 2))
          || (SampleCount % ((int)SAMPLING_FREQUENCY_HZ / 10) == 0)
          || (SampleCount == (int)TEST_SAMPLE_COUNT) )
        {   // show first and some selected values
            printf("%5d: %16.12fV; %16.12fV; %16.12fV\n", SampleCount, fVoltageV, dVoltageV, (double)ldVoltageV);
            /* format specifier %Lf does not work for 'long double' with Code::Blocks 16.01 / GCC 5.3.0 */
        }
    }
    printf("The expected value is %16.12fV.\n", (double)sinl(
        (long double)2.0 * (long double)M_PI * (long double)SINEWAVE_FREQUENCY_HZ * (long double)TEST_DURATION_S));
    return 0;
}
